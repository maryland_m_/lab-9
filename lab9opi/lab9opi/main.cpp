using namespace std;
#include <iostream>
#include <fstream>
#include <vector>
#include "strr.h"
#include <cstring>
#include "summup.h"
ifstream in("data.txt");


int amount = 0;
void outOneDirection(vector<Data*> vect, string dir)
{
    for (auto i : vect)
    {
        if (i->direction == dir)
            cout << i->day << ' ';
    }
}
void outOneDirection(vector<Data*> vect, float speed)
{
    for (auto i : vect)
    {
        if (i->speed == speed)
            cout << i->day << ' ';
    }
}


void ShakerSort(vector<Data>& a, bool comp(Data a, Data b)) {
    int m = a.size();
    int i, j, k;
    for (i = 0; i < m;) {
        for (j = i + 1; j < m; j++) {
            if (comp(a[j], a[j - 1]))
                swap(a[j], a[j - 1]);
        }
        m--;
        for (k = m - 1; k > i; k--) {
            if (a[k].speed < a[k - 1].speed)
                swap(a[k], a[k - 1]);
        }
        i++;
    }
};

static void merge(vector<Data>& buf, size_t left, size_t right, size_t middle, bool comp(Data a, Data b)) //
{
    if (left >= right || middle < left || middle > right) return;
    if (right == left + 1 && comp(buf[left], buf[right])) {
        swap(buf[left], buf[right]);
        amount++;
        return;
    }

    vector<Data> tmp(&buf[left], &buf[right] + 1);

    for (size_t i = left, j = 0, k = middle - left + 1; i <= right; ++i) {
        if (j > middle - left) {
            buf[i] = tmp[k++];
        }
        else if (k > right - left) {
            buf[i] = tmp[j++];
        }
        else {
            buf[i] = (!comp(tmp[j], tmp[k])) ? tmp[j++] : tmp[k++];
        }
    }
}

void MergeSort(vector<Data>& buf, size_t left, size_t right, bool comp(Data a, Data b)) //
{
    if (left >= right) return;

    size_t middle = left + (right - left) / 2;

    MergeSort(buf, left, middle, comp);
    MergeSort(buf, middle + 1, right, comp);
    merge(buf, left, right, middle, comp);
}

bool compOne(Data a, Data b)
{
    return a.speed < b.speed;
}
bool compTwo(Data a, Data b)
{
    if (a.direction < b.direction)
        return true;
    else
        if (a.direction == b.direction && a.mounth < b.mounth)
            return true;
        else
            if (a.direction == b.direction && a.mounth == b.mounth && (a.day < b.day))
                return true;
    return false;
}


int main()
{
    cout << "Laboratory work #9 GIT\n";
    cout << "Variant #6.\n";
    cout << "Author: Maria Kurnosova\n"
    cout << "Group: 14";
    return 0;
}
