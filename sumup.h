#ifndef SmUP
#define SmUP

#include "str.h"
#include <vector>
#include <iostream>
double midddle(std::vector<Data> vect, int mounth)
{
    float sum = 0;
    int total = 0;
    for(auto i : vect)
    {
        if(i.mounth == mounth){
            sum+=i.speed;
            total++;
        }

    }
    if(total <= 0)
        return -1;
    return float(sum/total);
}
#endif